'use strict';

module.exports = function(Analytics) {

    Analytics.observe('before save', function addDate(ctx, next){

        // ctx.instance is only defined if it's a new instance
        ctx.instance.datetime = Date.now();
        next();

    });

};
