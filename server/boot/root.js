'use strict';
var path = require('path');
var loopback = require('loopback');

module.exports = function(app) {

  //Decide what path to frontend is depending on development/production
  // if (env !== 'prod') {
  //   staticPath = path.resolve(__dirname, '../client/app/');
  //   console.log("Running app in development mode");
  // } else {
  //   staticPath = path.resolve(__dirname, '../dist/');
  //   console.log("Running app in production mode");
  // }

  var staticPath = null;
  staticPath = path.resolve(__dirname, '../../client/app/');
  app.use(loopback.static(staticPath));
  app.use('/api', loopback.rest());
  app.all('/*', function(req, res, next) {
      // Just send the index.html for other files to support HTML5Mode
      res.sendFile('index.html', { root: path.resolve(__dirname, '../../client/app/') });
  });
};
