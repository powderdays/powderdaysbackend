'use strict';

var xmlbuilder = require('xmlbuilder');
var fs = require('fs');
var stream = require('stream');
var zlib = require('zlib');

module.exports = function(server){

    if(process.argv.indexOf('sitemap')>0){
        console.log('generating sitemap')
    
        var room = server.models.room;
        var article = server.models.article;

        var filelist = []

        var urllist =['http://www.powderdays.xyz/', 'http://www.powderdays.xyz/about',
        'http://www.powderdays.xyz/legal', 'http://www.powderdays.xyz/blog'];

        var sm = fs.createWriteStream('client/app/sitemap/sitemap.xml');

        var xml = xmlbuilder.create('urlset');

        xml.attribute('xmlns', "http://www.sitemaps.org/schemas/sitemap/0.9");
        urllist.forEach(function(url){
            xml.element('url').element('loc', url);
        });

        var xmlstring = xml.end({ pretty: true, indent: '  ', newline: '\n', allowEmpty: false });
        sm.write(xmlstring);
        sm.end;

        filelist.push('sitemap.xml');


        
        const batch = 50000;
        
        var generateRoomSitemap = function(i) {
            console.log('Room sitemap: ',i)
            
            room.find({"limit": batch, "offset": i*batch,"fields":{'id':true}})
            .then(function(response){

                if(response.length>0){
                    var gz = zlib.createGzip();
                    gz.pipe(fs.createWriteStream('client/app/sitemap/sitemap_rooms_'+i+'.xml.gz'))

                    filelist.push('sitemap_rooms_'+i+'.xml.gz');
                    var xml = xmlbuilder.begin(function(chunk){gz.write(chunk);});
                    xml.dec('1.0', 'UTF-8')
                    var urlset = xml.ele('urlset', {'xmlns': "http://www.sitemaps.org/schemas/sitemap/0.9"});

                    response.forEach(function(room){
        
                        urlset.element('url').element('loc','http://www.powderdays.xyz/details/'+String(room.id)).up().up();

                    })
                    xml.end();
                    gz.end();

                    generateRoomSitemap(i+1);
                }
                else{
                    generateArticleSitemap();
                }
                
            })
            .catch(function(error) {console.log(error);});

        }

        var generateArticleSitemap = function() {
            
            article.find({"fields":{'id':true}})
            .then(function(response){

                
                var gz = zlib.createGzip();
                gz.pipe(fs.createWriteStream('client/app/sitemap/sitemap_articles.xml.gz'))

                filelist.push('sitemap_articles.xml.gz');
                var xml = xmlbuilder.begin(function(chunk){gz.write(chunk);});
                xml.dec('1.0', 'UTF-8')
                var urlset = xml.ele('urlset', {'xmlns': "http://www.sitemaps.org/schemas/sitemap/0.9"});

                response.forEach(function(article){
    
                    urlset.element('url').element('loc','http://www.powderdays.xyz/blog/'+String(article.id)).up().up();

                })
                xml.end();
                gz.end();

             
            
                generateSitemapIndex();
                
                
            })
            .catch(function(error) {console.log(error);});

        }

        var generateSitemapIndex = function(){
            var smi = fs.createWriteStream('client/app/sitemap/sitemapindex.xml');
            var xml = xmlbuilder.create('sitemapindex')
            xml.attribute('xmlns', "http://www.sitemaps.org/schemas/sitemap/0.9");

            filelist.forEach(function(file){
                xml.element('sitemap').element('loc','http://www.powderdays.xyz/sitemap/'+file);
            })

            var xmlstring = xml.end({ pretty: true, indent: '  ', newline: '\n', allowEmpty: false });
            smi.write(xmlstring);
            smi.end();

        }


        generateRoomSitemap(0);
        
        
    }
    
};