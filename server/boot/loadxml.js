'use strict';


module.exports = function(server){

    //Load the Loopback models
    var accommodation = server.models.accommodation;
    var offer = server.models.offer;
    var room = server.models.room;

    var downloadXML = function(){

        var ftp = require('ftp');
        var zlib = require("zlib");
        var fs = require('fs');

        var c = new ftp();
        c.on('ready', function() {

            c.get('Download/GB/stamm.xml.gz', function(err, stream) {
                if (err) throw err;
                stream.once('close', function() { 
                    console.log("Finished downloading stamm.xml.gz");
                });
                stream.pipe(zlib.createGunzip()).pipe(fs.createWriteStream('XML/stamm.xml'));
                });

            c.get('Download/GB/pakete.xml.gz', function(err, stream) {
                if (err) throw err;
                stream.once('close', function() { 
                    console.log("Finished downloading pakete.xml.gz");
                });
                stream.pipe(zlib.createGunzip()).pipe(fs.createWriteStream('XML/pakete.xml'));
                });

            c.end();
        });

        

        c.on('end', function(){
            loadAccommodations();
            });

        c.connect({
            host: "dataexchange.traveltrex.com",
            port: 21,
            user: "tt-p2405-01",
            password: "N1-TZ7p#T2a-rbu@18MJ"
        }, function(err){
            if(err) throw err;
        });

    }


    var loadAccommodations = function(){

        var fs = require('fs');
        var xpath = require('xpath');
        var dom = require('xmldom').DOMParser;

        // Load the master file of accommodations and rooms
        fs.readFile('XML/stamm.xml', 'utf8', function(err, xml){

            var master = new dom().parseFromString(xml);

            //To assign country later       
            var countryNodes = xpath.select("//country", master)
            

            countryNodes.forEach(function(element,index,array){
                var country = xpath.select("name/text()", element).toString().replace("<![CDATA[", "").replace("]]>", "");
                var countryCode = xpath.select("code/text()", element).toString().replace("<![CDATA[", "").replace("]]>", "");

                countryCodes[countryCode] = country;
            });

            //Find all <accomodation> nodes
            var accommodationNodes = xpath.select("//accomodation", master)


            //Loop through them and save to database 
            accommodationNodes.forEach(function(element,index,array){

                var accommodationID = xpath.select("ID/text()", element).toString();
                var name = xpath.select("name/text()", element).toString();
                var description = xpath.select("name/text()", element).toString();
                var country = xpath.select("ancestor::country/name/text()", element).toString();
                var resort = xpath.select("ancestor::resort/name/text()", element).toString();
                var resortDescription = xpath.select("ancestor::resort/description/text()", element).toString();
                var slopePlan = xpath.select("ancestor::resort/slope_plan_big/text()", element).toString();

                var region = xpath.select("ancestor::region/name/text()", element).toString();
                var bookingURL = xpath.select("deeplink_booking/text()", element).toString();

                var services = xpath.select("services/text()", element).toString();
                var services2 = xpath.select("services2/text()", element).toString();
                var photo = xpath.select("foto/text()", element).toString();

                // Parse services into a list of things
                var serviceIcons = {};
              
                serviceIcons['wifi']  = /\bwi[- ]*fi\b/i.test(services) ? true : false ;
                serviceIcons['spa'] = /\bspa\b/i.test(services) ? true : false ;
                serviceIcons['pool'] = /\bpool\b/i.test(services) ? true : false ;
                serviceIcons['tv'] = /\bTV\b/i.test(services) ? true : false ;
                serviceIcons['liftpass'] = /\blift[ ]?pass\b/i.test(services) ? true : false ;
                serviceIcons['snow'] = /\bsnow\b/i.test(services) ? true : false ;
                serviceIcons['parking'] = /\bparking\b/i.test(services) ? true : false ;
                serviceIcons['flights'] = true;
                serviceIcons['apres'] = true;

                //Use the Loopback API to create a new instance 
                accommodation.upsert({
                    "id":accommodationID.replace("<![CDATA[", "").replace("]]>", ""),
                    "name":name.replace("<![CDATA[", "").replace("]]>", "").replace("(anonymous)","").replace("[value price]",""),
                    "description":description.replace("<![CDATA[", "").replace("]]>", "").replace("(anonymous)","").replace("[value price]",""),
                    "country": country.replace("<![CDATA[", "").replace("]]>", ""),
                    "resort": resort.replace("<![CDATA[", "").replace("]]>", ""),
                    "resortDescription": resortDescription.replace("<![CDATA[", "").replace("]]>", ""),
                    "slopePlan": slopePlan.replace("<![CDATA[", "").replace("]]>", ""),
                    "region": region.replace("<![CDATA[", "").replace("]]>", ""),
                    "bookingURL": bookingURL.replace("<![CDATA[", "").replace("]]>", ""),
                    "services": services.replace("<![CDATA[", "").replace("]]>", ""),
                    "servicesIcons": serviceIcons,
                    "photo": photo.replace("<![CDATA[", "").replace("]]>", ""),
                    "updated": true

                } , function(err,obj){
                        if(err) console.log(err);
                    });

                //Now save each room in this accommodation

                var roomNodes = xpath.select("rooms/room",element);

                roomNodes.forEach(function(roomItem,index,array){
                    var roomID = xpath.select("ID/text()", roomItem).toString();
                    var name = xpath.select("name/text()", roomItem).toString();
                    var total_text = xpath.select("total_text/text()", roomItem).toString();
                    var type = xpath.select("type/text()", roomItem).toString();
                    var max_pax = xpath.select("max_pax/text()", roomItem).toString();
                    var photos_small = [xpath.select("foto_1/text()", roomItem).toString(),
                                        xpath.select("foto_2/text()", roomItem).toString(),
                                        xpath.select("foto_3/text()", roomItem).toString()];
                    var photos_big =   [xpath.select("foto_1_big/text()", roomItem).toString(),
                                        xpath.select("foto_2_big/text()", roomItem).toString(),
                                        xpath.select("foto_3_big/text()", roomItem).toString()];

                    if(name.indexOf("SC")>-1){
                        var board = "SC";
                    }
                    else if(name.indexOf("HB")>-1){
                        var board = "HB";
                    }
                    else if(name.indexOf("FB")>-1){
                        var board = "FB";
                    }
                    else{
                        var board = "";
                    }

                    var roomObj = {
                        "name":name.replace("<![CDATA[", "").replace("]]>", "").replace("(anonymous)","").replace("[value price]",""),
                        "total_text": total_text.replace("<![CDATA[", "").replace("]]>", ""),
                        "type": type.replace("<![CDATA[", "").replace("]]>", ""),
                        "board": board,
                        "max_pax": max_pax,
                        "photos_small":photos_small,
                        "photos_big": photos_big,
                        "id": roomID,
                        "accommodationID": accommodationID,
                        "updated": true
                    };

                    room.upsert(roomObj, function(err,obj){
                        if(err) console.log(err);
                    })
                })


            });

            console.log('Done saving accommodations')
            loadOffers();



        });

    };

    var loadOffers = function(){

        var counter = 0;

        var fs = require('fs');
        var xpath = require('xpath');
        var dom = require('xmldom').DOMParser;
        var readline = require('readline');

        var inPackage = false;
        var chunks = [];
        var lineReader = readline.createInterface({
          input: fs.createReadStream('XML/pakete.xml')
        });

        
        var indexOffer = 0 

        lineReader.on('close', function(){
            console.log('offers finished: ', numOffers);
            setTimeout(function(){
                console.log('deleting non-updated entries')
                accommodation.destroyAll({'updated':false})
                .then(function(){
                    return room.destroyAll({'updated':false});
                })
                .then(function(){
                    return offer.destroyAll({'updated':false});
                })
                .catch(function(error){

                    console.log(error)
                });

            }, 10000);

        })

        lineReader.on('line', function (line) {
             
            if(!inPackage){
                if(line.indexOf('<package_service>')>-1){
                    inPackage = true;
                    chunks.push(line);
                }
            }
            else{
                if(line.indexOf('</package_service>')<0){
                    chunks.push(line);
                }
                else{
                    chunks.push(line);
                    lineReader.pause();
                    inPackage = false;


                    //Now we can parse the extracted <package_services> object

                    var package_service = chunks.join("\n")
                    chunks = [];

                    var xml = new dom().parseFromString(package_service);

                    var roomID = xpath.select('//room_ID/text()', xml).toString();
                    var accommodationID = xpath.select('//accomodation_ID/text()', xml).toString();
                    var country = countryCodes[xpath.select('//country_code/text()', xml).toString()];

                    var offers = xpath.select('//allocation',xml);

                    
                    var offersObjArray = offers.map(function(item){

                        var id = xpath.select('offer_id/text()',item).toString();
                        
                        var datestr = xpath.select('ancestor::week/date/text()',item).toString();
                        var date = new Date(datestr.slice(0,4),datestr.slice(4,6),datestr.slice(6,8))
                        var nights = xpath.select('ancestor::week/duration/text()',item).toString();
                        var bookingURL = xpath.select('deeplink_booking/text()',item).toString();
                        var price = xpath.select('price/text()',item).toString();
                        var priceEUR = xpath.select('price_EUR/text()',item).toString();
                        var people = xpath.select('number/text()',item).toString().split('+')[0];

                        return {    
                            "id":id,
                            "roomID": roomID,
                            "accommodationID": accommodationID,
                            "country": country,
                            "nights": nights,
                            "date": date,
                            "bookingURL": "http://"+bookingURL.replace(/&amp;/g, '&')+"&code=2405",
                            "price": price,
                            "priceEUR": priceEUR,
                            "people": people,
                            "flights": true,
                            "updated": true

                        };
                    })

                    Promise.all(offersObjArray.map(function(offerObj){
                        // counter += 1; 
                        // console.log("Mongo requests +1: ", counter)
                        return offer.upsert(offerObj).then(function(res){
                            // counter -= 1;
                            // console.log("Mongo requests -1:, ", counter)
                            
                            return res;
                        });
                    }))
                    .then(function(response){
                        console.log('finished offers for room ', String(roomID))
                        indexOffer += 1;
                        setTimeout(function(){
                            lineReader.resume();
                        }, 100);
                        
                    })
                    .catch(function(err){
                        console.log(err);
                        indexOffer += 1;
                        lineReader.resume();
                    })
                    

                }
            }

            if(indexOffer>numOffers){
                lineReader.close();
            }
           

        });

    };

    var clearDatabase = function(){

        return accommodation.destroyAll()
        .then(function(){
            return offer.destroyAll();
        })
        .then(function(){
            return room.destroyAll();
        })
        .then(function(){
            downloadXML();
        });
    }; 

    var setUpdatedFalse = function(){
        return accommodation.updateAll({'updated':false})
        .then(function(){
            return offer.updateAll({'updated':false});
        })
        .then(function(){
            return room.updateAll({'updated':false})
        })
    }




    //Set up schedule or run immediately

    var scheduler = require('node-schedule');

    var countryCodes = {};

    if(process.env.NODE_ENV == 'production') var numOffers = Infinity;
    else var numOffers = Infinity;

    var daily = scheduler.scheduleJob('0 20 5 * * *', function(){
        console.log("downloading xml on schedule")
        //First set all data to 'updated: false'
        setUpdatedFalse()
        .then(function(){
            downloadXML();
        })
        .catch(function(err){
            console.log(err);
        })
        
    })

    if(process.argv.indexOf('clear')>0){

        console.log('clearing database and reloading xml')

        //First DELETE ALL RECORDS

        clearDatabase()
        .then(function(){
            downloadXML();
        })

        
    }
    else if(process.argv.indexOf('load')>0){
        console.log('reloading xml without clearing database')
        if(process.env.NODE_ENV == 'production') downloadXML();
        else loadAccommodations();
        
    }
    else if(process.argv.indexOf('test')>0){
          console.log('clearing database and reloading old xml file')

        //First DELETE ALL RECORDS
        accommodation.destroyAll()
        .then(function(){
            return offer.destroyAll();
        })
        .then(function(){
            return room.destroyAll();
        })
        .then(function(){
            loadAccommodations();
        });
    }
    else{
        console.log('not reloading xml')
    }


};
