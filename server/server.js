'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path')
// var env = require('get-env')({
//   test: 'test'
// });
// if (env !== 'prod'){
//   var heapdump = require('heapdump');
// }



var app = module.exports = loopback();


//Decide what path to frontend is depending on development/production
// if (env !== 'prod') {
//   staticPath = path.resolve(__dirname, '../client/app/');
//   console.log("Running app in development mode");
// } else {
//   staticPath = path.resolve(__dirname, '../dist/');
//   console.log("Running app in production mode");
// }


app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
