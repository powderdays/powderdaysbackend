This repo contains the Loopback server and frontend client of Powderdays.xyz
The client is contained in /client/app. Running the Loopback server (node .)
also serves the frontend. 

Some tasks are available using gulp. First install:
npm install --global gulp-cli
Then, after possibly doing npm install, you can run

- gulp bower after installing any new packages with bower, to wire the dependency into index.hmtl
- gulp lbServices to generate new LoopBack Angular SDK in case you change a model
- gulp server both to start up mongo and the server
- more to come