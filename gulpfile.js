var gulp = require('gulp');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var rename = require('gulp-rename');
var loopbackAngular = require('gulp-loopback-sdk-angular');
var wiredep = require('wiredep').stream;
var sitemap = require('gulp-sitemap');
 

gulp.task('default', function() {
  // place code for your default task here
});

//Starts mongo and node
gulp.task('server', function (cb) {

    const mongo = spawn('mongod');
    mongo.stdout.on('data',function(data){
        console.log('MONGO: %s', data);
    })
    mongo.stderr.on('data',function(data){
        console.log('MONGOERR: %s', data);
    })

    const lbserver = spawn('nodemon',['.']);
    lbserver.stdout.on('data',function(data){
        console.log('LOOPBACK: %s', data);
    })
    lbserver.stderr.on('data',function(data){
        console.log('LOOPBACK: %s', data)
    });

});

//Generates Loopback Angular services
gulp.task('lbServices', function () {
    return gulp.src('./server/server.js')
    .pipe(loopbackAngular())
    .pipe(rename('lb-services.js'))
    .pipe(gulp.dest('./client/js'));
});

//Wires up Bower packages into index.html
gulp.task('bower', function () {
  gulp.src('./client/app/index.html')
    .pipe(wiredep())
    .pipe(gulp.dest('./client/app/'));
});

gulp.task('sitemap', function(){
    gulp.src('client/app/index.html',{
        read: false
    })
    .pipe(sitemap({
        siteUrl: 'http://www.powderdays.xyz'
    }))
    .pipe(gulp.dest('client/app/'))
})