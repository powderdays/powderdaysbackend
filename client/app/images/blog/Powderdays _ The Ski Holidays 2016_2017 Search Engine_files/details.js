'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('DetailsCtrl', function ($scope, $location, $routeParams, favouritesService, resultsService) {
    
   // var detailId = $routeParams.detailId; 
    $scope.url = $location.path();

  
    
    $scope.offers = resultsService.offers;
    $scope.details = $scope.offers[0];
    $scope.details.photos = [$scope.details.accommodation.photo].concat($scope.details.room.photos_big);

    // After this, PhotoSwipe stuff

    var openPhotoSwipe = function() {
        var pswpElement = document.querySelectorAll('.pswp')[0];

        var items = $scope.details.photos.map(function(obj){
          return {
            src: obj,
            w: 640,
            h: 480
          }
        });
        
        // define options (if needed)
        var options = {
                 // history & focus options are disabled on CodePen        
            history: false,
            focus: false,

            showAnimationDuration: 0,
            hideAnimationDuration: 0
            
        };
        
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // openPhotoSwipe();

    document.getElementById('open').onclick = openPhotoSwipe;
    document.getElementById('open2').onclick = openPhotoSwipe;
    
    $scope.clickStar = function(result){ return favouritesService.clickStar(result);}
    $scope.isFavourite = function(result){ return favouritesService.isFavourite(result);}


  });


