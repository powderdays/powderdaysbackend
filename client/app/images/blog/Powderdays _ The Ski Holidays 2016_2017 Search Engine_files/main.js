'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('MainCtrl', function ($scope, searchService, $location) {
    
    
    $scope.clickCountry = function(country, nights, people, date){
    searchService.parameters.country = country;
    searchService.parameters.nights = nights;
    searchService.parameters.people = people;
    searchService.parameters.date = date;
      $location.url('/results')
      
    }
    
  });
