'use strict';

/**
 * @ngdoc overview
 * @name angularYeomanApp
 * @description
 * # angularYeomanApp
 *
 * Main module of the application.
 */
angular
  .module('angularYeomanApp', [
    'firebase',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'lbServices',
    'ngclipboard',
    'angular.filter',
    'rzModule'
      ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/results', {
        templateUrl: 'views/results.html',
        controller: 'ResultsCtrl',
        controllerAs: 'resCtrl'
      })
      .when('/details', {
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl',
        controllerAs: 'detailsCtrl'
      })
      .when('/favourites', {
        templateUrl: 'views/favourites.html',
        controller: 'FavouritesCtrl',
        controllerAs: 'favourites'
      })
        .when('/articles', {
        templateUrl: 'views/articles.html',
        controller: 'ArticlesCtrl',
        controllerAs: 'articles'
      })
      .when('/articles2', {
        templateUrl: 'views/articles2.html',
        controller: 'ArticlesCtrl',
        controllerAs: 'articles2'
      })
      .when('/articles3', {
        templateUrl: 'views/articles3.html',
        controller: 'ArticlesCtrl',
        controllerAs: 'articles3'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'ArticlesCtrl',
        controllerAs: 'articles'
      })
      .when('/legal', {
        templateUrl: 'views/legal.html',
        controller: 'LegalCtrl',
        controllerAs: 'legal'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(['$httpProvider', function($httpProvider) {
          $httpProvider.defaults.useXDomain = true;
          delete $httpProvider.defaults.headers.common['X-Requested-With'];
      }
  ])

  .run(function($cookies, $firebaseObject){
    // Check whether userId cookie exists, otherwise create one and link to Firebase
    
    var userCookie = $cookies.get('userId')
    console.log('old cookie: '+userCookie)

    //If there is no userCookie, generate one
    if(typeof userCookie === 'undefined'){

      var ref = firebase.database().ref("users")
      var data = $firebaseObject(ref);

      //Push new user without email specified
      var newUserId = ref.push({'email':''});
      $cookies.put('userId', $firebaseObject(newUserId).$id);
      console.log('new cookie: '+$cookies.get('userId'))
      
    }
    
  });
