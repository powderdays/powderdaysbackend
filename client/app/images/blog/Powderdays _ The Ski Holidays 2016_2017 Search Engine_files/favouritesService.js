angular.module('angularYeomanApp')
.service('favouritesService',function($cookies, $firebaseObject){
    
    
    var userId = $cookies.get('userId');
    var refUser = firebase.database().ref("users/"+userId)
    var user = $firebaseObject(refUser);
    
    
    this.isFavourite = function(result){
        
        if(user.hasOwnProperty('favourites')){

            if(user['favourites'].indexOf(result.$id)>-1){
                return true;  
            }
            else{
                return false;
            }
            
        }
        else{
            return false
        }
        

    }
    
    this.clickStar = function(result){
        
        if(this.isFavourite(result)){
            user['favourites'].splice(user['favourites'].indexOf(result.$id),1)
        }
        else if(user.hasOwnProperty('favourites')){
            user['favourites'].push(result.$id);
        }
        else {
            user.favourites = [result.$id];
        }

        // Save changes to Firebase
        user.$save(); 

    }


});