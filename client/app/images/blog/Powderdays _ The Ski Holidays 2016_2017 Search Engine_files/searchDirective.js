'use strict';

angular.module('angularYeomanApp')
.directive('searchDirective', function(){
    return {
        templateUrl: "views/search.html",
        controller: 'searchDirectiveController'
    }
})
.controller('searchDirectiveController', function($scope, $route, $location, searchService){

    $scope.parameters = searchService.parameters;
    
    $scope.dateOptions = {
        showWeeks:false,
        minDate: new Date(2016,10,21),
        initDate: new Date(2016,10,21)
    };
    
    $scope.openDate = function(){
        $scope.dateOpened = true;
    }
    
    // Need this function so that the results view reloads if we do a new search
    // when we're already in the results view
    $scope.searchSubmit = function(){
        if ($location.path() == '/results'){
            $route.reload();
        }
        else{
            $location.path('/results')
        }
    }

})
.directive('searchDirective2', function(){
    return {
        templateUrl: "views/search2.html",
        controller: 'searchDirective2Controller'
    }
})
.controller('searchDirective2Controller', function($scope, $route, $location, searchService){

    $scope.parameters = searchService.parameters;
    
    $scope.dateOptions = {
        showWeeks:false,
        minDate: new Date(2016,10,21),
        initDate: new Date(2016,10,21)
    };
    
    $scope.openDate = function(){
        $scope.dateOpened = true;
    }
    
    // Need this function so that the results view reloads if we do a new search
    // when we're already in the results view
    $scope.searchSubmit = function(){
        if ($location.path() == '/results'){
            $route.reload();
        }
        else{
            $location.path('/results')
        }
    }

})
.service('searchService', function(){

    this.parameters = {
        country : '',
        date: new Date(2016,10,21),
        nights: '7',
        people: null,
        budget: null
    };

});