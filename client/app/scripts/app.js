'use strict';

/**
 * @ngdoc overview
 * @name angularYeomanApp
 * @description
 * # angularYeomanApp
 *
 * Main module of the application.
 */
angular
  .module('angularYeomanApp', [
    'firebase',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'lbServices',
    'ngclipboard',
    'angular.filter',
    'rzModule'
      ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/results', {
        templateUrl: 'views/results.html',
        controller: 'ResultsCtrl'
      })
      .when('/offers/:offerId', {
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl'
      })
      .when('/rooms/:roomId', {
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl'
      })
      .when('/favourites', {
        templateUrl: 'views/favourites.html',
        controller: 'FavouritesCtrl'
      })
      .when('/blog', {
        templateUrl: 'views/bloglist.html',
        controller: 'BlogCtrl'
      })
      .when('/blog/:blogId', {
        templateUrl: 'views/blog.html',
        controller: 'BlogCtrl'
      })
      .when('/bloginput', {
        templateUrl: 'views/bloginput.html',
        controller: 'BloginputCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html'
      })
      .when('/legal', {
        templateUrl: 'views/legal.html'
      })
        .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

      //Use HTML5 mode, to remove the # from url
      $locationProvider.html5Mode(true);

  })
  .config(['$httpProvider', function($httpProvider) {
          $httpProvider.defaults.useXDomain = true;
          delete $httpProvider.defaults.headers.common['X-Requested-With'];
      }
  ])

  .run(function($cookies, $firebaseObject){
    // Check whether userId cookie exists, otherwise create one and link to Firebase
    
    var userCookie = $cookies.get('userId')
    console.log('old cookie: '+userCookie)

    //If there is no userCookie, generate one
    if(typeof userCookie === 'undefined'){

      var ref = firebase.database().ref("users")
      var data = $firebaseObject(ref);

      //Push new user without email specified
      var newUserId = ref.push({'email':''});
      $cookies.put('userId', $firebaseObject(newUserId).$id);
      console.log('new cookie: '+$cookies.get('userId'))
      
    }
    
  });
