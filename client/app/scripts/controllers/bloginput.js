'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:ArticlesCtrl
 * @description
 * # ArticlesCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('BloginputCtrl', function ($scope, Article) {

    $scope.article = {};

    $scope.submit  = function(){
        Article.create($scope.article);
    }

  });
