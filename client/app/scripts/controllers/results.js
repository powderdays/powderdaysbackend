'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('ResultsCtrl', function ($scope, $location, searchService, favouritesService, resultsService, Offer) {
    
    // var userId = $cookies.get('userId');
    // var refUser = firebase.database().ref("users/"+userId)
    // var user = $firebaseObject(refUser);

    $scope.isCollapsed = true;

    $scope.loadingState = 0;

    var calculateCount = function(){
        $scope.countFiltered = $scope.results.map(function(element){
            return element.price>=$scope.slider.minValue && element.price <= $scope.slider.maxValue ? element.accommodationID : null ;
        }).filter(function(value,index,array){return value === null ? false : array.indexOf(value) === index;} ).length
    }

    var searchParams = searchService.parameters;
    var datelow = new Date(searchParams.date);
    var datehigh = new Date(searchParams.date);
    datelow.setDate(datelow.getDate()-7);
    datehigh.setDate(datehigh.getDate()+7);

    $scope.destination = searchParams.country;
    $scope.date = searchParams.date;

    var searchFilter = {
            limit: 10, 
            offset: 0,
            order: 'accommodationID ASC',
            where: {
                // country: searchParams.country,
                nights: parseInt(searchParams.nights),
                people: searchParams.people,
                date: {between: [datelow,datehigh]}
            },
            include: ['room','accommodation']
        };

    if(searchParams.country !== '' && searchParams.country !== 'Anywhere' ){
        searchFilter['where']['country'] = searchParams.country;
    }
    if(searchParams.budget){
        searchFilter['where']['price'] = {'lt': searchParams.budget+1};
    }
    if(!searchParams.people){
        searchFilter['where']['people'] = 2
    }


    Offer.find({
        filter: searchFilter
    })
    .$promise
    .then(function(response){
        if(response.length == 0){
            $scope.loadingState = 2;
        }
        else{
            $scope.loadingState = 1;
            $scope.results = response;

            // Budget slider options
            $scope.slider = {
                minValue: Math.min.apply(Math, response.map(function(el){return el.price})),
                maxValue: Math.max.apply(Math, response.map(function(el){return el.price})),
                options: {

            
                    floor: Math.min.apply(Math, response.map(function(el){return el.price})),
                    ceil: Math.max.apply(Math, response.map(function(el){return el.price})),
                    step: 1,
                    onChange: function(sliderId, modelValue, highValue, pointerType){
                        calculateCount();
                    }
                }
            };

            calculateCount();
            if($scope.countFiltered<10){
                $scope.loadMore();
            }
            

        }
       
    })
    .catch(function(error){
        console.log(error);
    });


    $scope.finished = false;

    $scope.loadMore = function(){

        //Load until the number of results crosses a multiple of 10
        var resultsGoal = $scope.countFiltered - $scope.countFiltered % 10 + 10 ;

        var load10more = function(){
            console.log($scope.results.length)
            searchFilter['offset'] = searchFilter['offset'] + 10;
            Offer.find({
                    filter: searchFilter
            })
            .$promise
            .then(function(data){
                $scope.results.push.apply($scope.results, data);
                calculateCount();
                if(data.length<10){ 
                    $scope.finished = true;
                }
                if($scope.countFiltered < resultsGoal && !$scope.finished){
                    load10more();
                }
            })
            .catch(function(error){
                console.log(error);
            });
        }

        load10more();
        
        
    }
    
    
    // console.log("searchParams: "+JSON.stringify(searchParams))


    // Options for sorting the results
    $scope.sorting = [
    {
        property: "cost",
        reverse: true
    },
    {
        property: "cost",
        reverse: false
    }];
    $scope.sortSelect=null;

    $scope.byRange = function (fieldName, minValue, maxValue) {
      if (minValue === undefined) minValue = 0;
      if (maxValue === undefined) maxValue = 1000;

      return function predicateFunc(item) {
        return minValue <= item[fieldName] && item[fieldName] <= maxValue;
      };
    };

    $scope.clickResult = function(offers){
        resultsService.offers = offers;
    }

    $scope.getMinPrice = function(offers){
        return Math.min.apply(Math, offers.map(function(el){return el.price})); 
    }

    
    $scope.clickStar = function(result){ return favouritesService.clickStar(result);}
    $scope.isFavourite = function(result){ return favouritesService.isFavourite(result);}

    
  });
