'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('ContactCtrl', function ($scope, Contact) {

    $scope.data = {};

    $scope.submit = function(){

        console.log($scope.data);

        Contact.create($scope.data).$promise
        .then(function(res){
            console.log('success')
            window.alert("Thank you for contacting Powderdays, we will try our best to answer your query within 24 hours!")
        })
        .catch(function(err){
            console.log(err);
        });

    }

  });
