'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('DetailsCtrl', function ($scope, $location, $routeParams, favouritesService, resultsService, Offer, Room, Bid) {
    
    //Check whether the link is just for a room/accommodation, or for a specific offer,
    //in which case the cost and date should be displayed
    if($routeParams.offerId){
        console.log('this is an offer')
        var offerId = $routeParams.offerId;
    }
    if($routeParams.roomId){
        console.log('this is a room')
        var roomId = $routeParams.roomId;
    }
    
    //Get the current URL to be able to share with friends 
    $scope.url = $location.absUrl();

    //If resultsService does have 

    if(offerId){
        Offer.findById({ id: offerId ,filter:{include: ['room','accommodation']}})
            .$promise
            .then(function(offer){ 
                $scope.details = offer;
                $scope.details.photos = [offer.accommodation.photo].concat(offer.room.photos_big);
                console.log($scope.details)
            });
    }
    else if(roomId){
        Room.findById({ id: roomId ,filter:{include: 'accommodation'}})
            .$promise
            .then(function(room){ 
                $scope.details = {};
                $scope.details.accommodation = room.accommodation;
                $scope.details.room = room;
                $scope.details.photos = [room.accommodation.photo].concat(room.photos_big);
                $scope.details.bookingURL = room.accommodation.bookingURL;
                console.log($scope.details)
            });
    }

    // if(resultsService.offers.length>0){
    //     $scope.offers = resultsService.offers;
    //     $scope.details = $scope.offers[0];
    //     $scope.details.photos = [$scope.details.accommodation.photo].concat($scope.details.room.photos_big);
    // }
    // else{
        
    // }

    $scope.bid = {
        email: null,
        price: null
    };

    $scope.makeOffer = function(){

        Bid.create({
            offerId: $scope.details.id,
            price: $scope.bid.price,
            email: $scope.bid.email
        }).$promise
        .then(function(response){
            window.alert('Thank you for making an offer!')
        })
        .catch(function(err){
            console.log(err);
        })
    }
    
    

    // After this, PhotoSwipe stuff

    $scope.openPhotoSwipe = function(index) {
        //How to use index?
        var pswpElement = document.querySelectorAll('.pswp')[0];

        var items = $scope.details.photos.map(function(obj){
          return {
            src: obj,
            w: 640,
            h: 480
          }
        });
        
        // define options (if needed)
        var options = {
                 // history & focus options are disabled on CodePen 
            index: index,       
            history: false,
            focus: false,

            showAnimationDuration: 0,
            hideAnimationDuration: 0
            
        };
        
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    
    $scope.clickStar = function(result){ return favouritesService.clickStar(result);}
    $scope.isFavourite = function(result){ return favouritesService.isFavourite(result);}


  });

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
        $('#sticky-anchor').height($('#sticky').outerHeight());
    } else {
        $('#sticky').removeClass('stick');
        $('#sticky-anchor').height(0);
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});

var dir = 1;
var MIN_TOP = 200;
var MAX_TOP = 350;

function autoscroll() {
    var window_top = $(window).scrollTop() + dir;
    if (window_top >= MAX_TOP) {
        window_top = MAX_TOP;
        dir = -1;
    } else if (window_top <= MIN_TOP) {
        window_top = MIN_TOP;
        dir = 1;
    }
    $(window).scrollTop(window_top);
    window.setTimeout(autoscroll, 100);
}

