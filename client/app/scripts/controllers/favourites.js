'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
 angular.module('angularYeomanApp')
 .controller('FavouritesCtrl', function ($scope, $cookies, $firebaseObject) {

    var userId = $cookies.get('userId');
    var userRef = firebase.database().ref("users/"+userId);
    var userData = $firebaseObject(userRef);
    $scope.results = null;

    // Options for sorting the results
    $scope.sorting = [
    {
        property: "cost",
        reverse: true
    },
    {
        property: "cost",
        reverse: false
    }];
    $scope.sortSelect = null;

    // Retrieve a user's favourited results
    userData.$loaded().then(function(user){

      var favouritesRefArray = user.favourites.map(function(objId){
        return firebase.database().ref().child("results/"+objId);
        });

      var favouritesArray = favouritesRefArray.map(function(ref){
        return $firebaseObject(ref);
      })

      $scope.results = favouritesArray;
        
    })

    
});
