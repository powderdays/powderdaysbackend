'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularYeomanApp
 */
 angular.module('angularYeomanApp')
 .controller('HeaderCtrl', function ($scope, $cookies, $firebaseObject) {

    var userId = $cookies.get('userId');
    var userRef = firebase.database().ref("users/"+userId);
    $scope.userData = $firebaseObject(userRef);
    $scope.results = null;

    $scope.warning = function(text) {
        alert(text);
    }

//    userData.$loaded().then(function(){console.log(userData)})
    


});
