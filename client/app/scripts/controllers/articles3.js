'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:ArticlesCtrl
 * @description
 * # ArticlesCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('ArticlesCtrl', function ($scope, $firebaseObject) {

    var ref = firebase.database().ref().child("testdata");
    // download the data into a local object
    $scope.data = $firebaseObject(ref);
    // putting a console.log here won't work, see below


  });
