'use strict';

/**
 * @ngdoc function
 * @name angularYeomanApp.controller:ArticlesCtrl
 * @description
 * # ArticlesCtrl
 * Controller of the angularYeomanApp
 */
angular.module('angularYeomanApp')
  .controller('BlogCtrl', function ($scope, $sce, $routeParams, Article) {

    if($routeParams.blogId){
        Article.findById({id: $routeParams.blogId}).$promise
        .then(function(article){
            $scope.article = article;
            $scope.body = $sce.trustAsHtml(article.body);
        })
    }
    else{
        Article.find({"limit":10}).$promise
        .then(function(articles){
            $scope.articles = articles;
        })
    }

  });
